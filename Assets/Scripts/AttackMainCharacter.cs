﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackMainCharacter : MonoBehaviour
{
    WorldManagement WM;
    public int numberOfLives = 20;
    GameObject player;
    private Vector3 direction;
    private float step;
    public float speed = 3;
    public Transform explosionPrefab;
    private float initialYPos;

    // Use this for initialization
    void Start()
    {
        WM = GameObject.Find("ARCamera").GetComponent<WorldManagement>();
        step = speed * Time.deltaTime;
        player = GameObject.FindGameObjectWithTag("MainCharacter");
        initialYPos = this.transform.position.y;
    }

    // Update is called once per frame
    void Update()
    {
        if (WM.gameIsRunning)
        {
            player = GameObject.FindGameObjectWithTag("MainCharacter");
            direction = (new Vector3(player.transform.position.x, 0, player.transform.position.z))
                - (new Vector3(this.transform.position.x, 0, this.transform.position.z));
            if (Vector3.Distance(this.transform.position, player.transform.position) < 30)
            {
                if (Vector3.Distance(this.transform.position, player.transform.position) > 12)
                {
                    //walk
                    this.GetComponent<Animator>().SetBool("skeleton_attack", false);
                    this.GetComponent<Animator>().SetBool("skeleton_walk", true);
                    this.GetComponent<Animator>().SetBool("skeleton_idle", false);

                    this.transform.rotation = Quaternion.Slerp(this.transform.rotation,
                                                            Quaternion.LookRotation(direction), 1);
                    this.transform.Translate(0, 0, step);
                }
                else
                {
                    //attack
                    this.GetComponent<Animator>().SetBool("skeleton_attack", true);
                    this.GetComponent<Animator>().SetBool("skeleton_walk", false);
                    this.GetComponent<Animator>().SetBool("skeleton_idle", false);

                    this.transform.rotation = Quaternion.Slerp(this.transform.rotation,
                                                            Quaternion.LookRotation(direction), 1);
                }
            }
            else
            {
                //idle
                this.GetComponent<Animator>().SetBool("skeleton_attack", false);
                this.GetComponent<Animator>().SetBool("skeleton_walk", false);
                this.GetComponent<Animator>().SetBool("skeleton_idle", true);
            }
            this.transform.position = new Vector3(transform.position.x, initialYPos, transform.position.z);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (WM.gameIsRunning)
        {
            string colliderObjectTag = other.transform.gameObject.tag;
            if (colliderObjectTag.Equals("MainCharacterWeapon"))
            {
                if (numberOfLives > 0)
                {
                    numberOfLives--;
                }
                else
                {
                    Instantiate(explosionPrefab, transform.position, transform.rotation);
                    WM.numberOfEnemies--;
                    Destroy(this);
                }
            }
        }
    }
}
