﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RemoteInteraction : MonoBehaviour
{
    WorldManagement WM;
    GameObject sphere;
    Text txt;

    private GameObject player;
    private Vector3 playerNewPosition;

    private Vector3 remoteBeamPosition;
    // Use this for initialization
    void Start()
    {
        WM = GameObject.Find("ARCamera").GetComponent<WorldManagement>();
        player = GameObject.Find("samuzai_animation_ok");

        sphere = GameObject.Find("CollisionSphere");
        sphere.transform.localScale = new Vector3(1, 1, 1);
        sphere.transform.position = player.transform.position;
        txt = GameObject.Find("InteractionText").GetComponent<Text>();

        remoteBeamPosition = this.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        player = GameObject.Find("samuzai_animation_ok");
    }

    private void OnTriggerEnter(Collider other)
    {
        if (WM.gameIsRunning)
        {
            string colliderObjectName = other.transform.gameObject.name;
            if (colliderObjectName.Equals("Ground"))
            {
                txt.text = colliderObjectName;
            }
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (WM.gameIsRunning)
        {
            string colliderObjectName = other.transform.gameObject.name;
            if (colliderObjectName.Equals("Ground"))
            {
                Vector3 collisionPos = other.gameObject.GetComponent<Collider>().ClosestPointOnBounds(transform.position);
                txt.text = colliderObjectName + " - " + collisionPos;
                sphere.transform.position = new Vector3(collisionPos.x, 4.5f, collisionPos.z);

            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (WM.gameIsRunning)
        {
            string colliderObjectName = other.transform.gameObject.name;
            sphere.transform.position = player.transform.position;
            if (colliderObjectName.Equals("Ground"))
            { txt.text = "No collision"; 
            }
        }
    }
}
