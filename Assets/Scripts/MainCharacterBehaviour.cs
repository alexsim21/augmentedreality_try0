﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainCharacterBehaviour : MonoBehaviour
{

    private Transform target;
    private Vector3 direction;
    private float step;
    public float speed;
    private bool allowToWalk;
    Text txt;
    private float initialYPos;

    WorldManagement WM;

    // Use this for initialization
    void Start()
    {
        WM = GameObject.Find("ARCamera").GetComponent<WorldManagement>();

        initialYPos = this.transform.position.y;
        txt = GameObject.Find("InteractionText").GetComponent<Text>();

        allowToWalk = true;
        step = speed * Time.deltaTime;
        target = GameObject.Find("CollisionSphere").transform;
    }

    // Update is called once per frame
    void Update()
    {
        if (WM.gameIsRunning)
        {
            if (allowToWalk)
            {
                direction = (new Vector3(target.position.x, 0, target.position.z)) - (new Vector3(this.transform.position.x, 0, this.transform.position.z));
                this.GetComponent<Animator>().SetBool("samurai_attack", enemyIsAround());
                if (enemyIsAround())
                {
                    //attack
                    this.GetComponent<Animator>().SetBool("samurai_walk", false);
                    this.GetComponent<Animator>().SetBool("samurai_idle", false);
                    //transform.position = Vector3.MoveTowards(transform.position, target.position, step);
                    this.transform.rotation = Quaternion.Slerp(this.transform.rotation,
                                                            Quaternion.LookRotation(direction), 1);
                    if (Vector3.Distance(this.transform.position, target.transform.position) > 12)
                    {
                        //walk and attack
                        this.transform.Translate(0, 0, step);
                    }

                    //Fight the skeleton
                }
                else
                {
                    if (Vector3.Distance(target.position, transform.position) > 5)
                    {
                        //walk
                        this.GetComponent<Animator>().SetBool("samurai_walk", true);
                        this.GetComponent<Animator>().SetBool("samurai_idle", false);

                        //transform.position = Vector3.MoveTowards(transform.position, target.position, step);
                        this.transform.rotation = Quaternion.Slerp(this.transform.rotation,
                                                            Quaternion.LookRotation(direction), 1);
                        this.transform.Translate(0, 0, step);
                    }
                    else
                    {
                        //idle
                        this.GetComponent<Animator>().SetBool("samurai_walk", false);
                        this.GetComponent<Animator>().SetBool("samurai_idle", true);
                    }
                }
            }
            else
            {
                //reset position
                this.transform.position = new Vector3(0, initialYPos, 0);
                txt.text = "Reseting player to initial position";
            }
            this.transform.position = new Vector3(transform.position.x, initialYPos, transform.position.z);

            verifyEndOfTheGame();
        }
    }

    private bool enemyIsAround()
    {
        //TODO:
        GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");
        foreach (GameObject enemy in enemies)
        {
            if (Vector3.Distance(this.transform.position, enemy.transform.position) < 20)
            {
                target = enemy.transform;
                return true;
            }
        }
        target = GameObject.Find("CollisionSphere").transform;
        return false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (WM.gameIsRunning)
        {
            string colliderObjectName = other.transform.gameObject.name;
            if (colliderObjectName.Equals("Ground"))
            {
                allowToWalk = true;
            }
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (WM.gameIsRunning)
        {
            string colliderObjectName = other.transform.gameObject.name;
            if (colliderObjectName.Equals("Ground"))
            {
                allowToWalk = true;
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (WM.gameIsRunning)
        {
            string colliderObjectName = other.transform.gameObject.name;
            if (colliderObjectName.Equals("Ground"))
            {
                allowToWalk = false;
            }
        }
    }

    public void verifyEndOfTheGame() {
       
        if (WM.numberOfEnemies == 0)
        {
            //Level Clear
            txt.text = "Congratulations, you've managed to finish the leve!!! Press back/escape to exit the game";
            WM.gameIsRunning = false;
            this.GetComponent<Animator>().SetBool("samurai_attack", false);
            this.GetComponent<Animator>().SetBool("samurai_walk", false);
            this.GetComponent<Animator>().SetBool("samurai_idle", true);
        }
    }
}
