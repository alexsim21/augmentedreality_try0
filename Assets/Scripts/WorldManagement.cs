﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WorldManagement : MonoBehaviour
{
    public bool gameIsRunning;
    public int numberOfEnemies = 6;
    // Use this for initialization
    void Start()
    {
        gameIsRunning = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Escape))
        {
            Text txt = GameObject.Find("InteractionText").GetComponent<Text>();
            txt.text = "App will exit";
            Application.Quit();
        }
    }
}
